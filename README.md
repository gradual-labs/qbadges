---
version: 1.1.2.4
---
<meta charset="utf8"/>
# holoBadges

holoBadge are little "recognizable" image for the community
a simple way to communicate key information to our members

we currently uses unprotected ones.
however the next release will bring protection

This is a fundamental difference with other conventional badge is
the they are "secured" and can't be spoofed.

exemples:

#### Projects
<br>[![](https://img.shields.io/badge/project-holoSphere_🌕-purple.svg?style=flat-square&logoColor=gold&logo=GraphQL)](http://holosphere.ml/)

#### Attribution
<br>[![bring](https://img.shields.io/badge/project-blockRing™-darkgreen.svg?style=flat-square&logoColor=gold&logo=CodeSandbox)](http://blockRing™.ml/)
![vi](https://img.shields.io/badge/made_w/-♡-red.svg?style=flat-square&logo=Vim)
[![michelc](https://img.shields.io/badge/by-{{site.creator_snake}}-purple.svg?style=flat-square)]({{site.search_url}}=!g+%22{{site.creator}}%22+site:.ml)
[![](https://img.shields.io/badge/made%20for-{{site.org_snake}}-blue.svg?style=flat-square)]({{site.org_url}})

#### contact
<br>[![](https://img.shields.io/badge/telegram-@cryptomgc-blue.svg?style=flat-square)](http://t.me/cryptomgc)
<br>[![](https://img.shields.io/badge/email-@michelc-blue.svg?style=flat-square)](mailto:michelc@gc-bank.org)

#### freq
<br>[![](https://img.shields.io/badge/freq.-7.8KHz-blue.svg?style=flat-square)](http://holotools.ml/play?freq=7.8KHz)

#### versions
<br>![version](https://img.shields.io/badge/version-{{page.version}}-blue.svg)
<br>[![GitHub commit](https://img.shields.io/github/last-commit/michel47/bin)](https://github.com/michel47/bin/commits/master)
<br>![Version](https://img.shields.io/packagist/v/michel/bin.svg)
<br>[![Github all releases](https://img.shields.io/github/downloads/Gradual-Quanta/minichain/total.svg)](https://GitHub.com/Gradual-Quanta/minichain/releases/)

#### issues
<br>[![GitHub issues open](https://img.shields.io/github/issues/adminkit/adminkit.svg)](https://github.com/adminkit/adminkit/issues?q=is%3Aopen+is%3Aissue)
<br>[![GitHub issues closed](https://img.shields.io/github/issues-closed-raw/adminkit/adminkit.svg)](https://github.com/adminkit/adminkit/issues?q=is%3Aissue+is%3Aclosed)

#### code stats
<br>[![GitHub contributors](https://img.shields.io/github/contributors/civetweb/civetweb.svg)](https://github.com/civetweb/civetweb/blob/master/CREDITS.md)
<br>[![Coveralls](https://img.shields.io/coveralls/civetweb/civetweb.svg?maxAge=3600)]()
<br>![Mozilla HTTP Observatory Grade](https://img.shields.io/mozilla-observatory/grade-score/:domain)


<br>[![markdown](https://img.shields.io/badge/format-markdown-ffaabb.svg?style=flat-square&logo=Markdown&logoColor=ffaabb)](http://markdown.org)
<br>[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
<br>[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

#### donations: 
<br>![Codetally](https://img.shields.io/codetally/michel47/bin)
<br>[![donate](https://img.shields.io/badge/Donate-PayPal-green.svg)]({{DUCK}}=donate+to+mychelium)
<br>![help](https://img.shields.io/badge/donate-%E2%99%A1-ff39e5.svg?style=flat)



#### rating:
<img alt="WordPress Theme Rating badge" src="https://img.shields.io/badge/rating-%E2%98%85%E2%98%85%E2%98%85%E2%AF%AA%E2%98%86-brightgreen">

#### Licenses
<br>[![License](https://img.shields.io/badge/license-HKL-blue.svg)](https://opensource.org/licenses/HKL)
<br><img alt="GitHub Licence" src="https://img.shields.io/github/license/michel47/web?style=flat-square">
<br>![GitHub](https://img.shields.io/github/license/michel47/web?style=flat-square)

#### tracking & SEO
<br>[![spot=:spot](https://img.shields.io/badge/spotted%20at-:spot-cyan.svg?style=flat-square)](https://duckduckgo.com/?q=!g+spot+:spot)
<br>[![{{site.tag}}](https://img.shields.io/badge/tag-{{site.tag}}-yellow.svg?style=flat-square)](https://duckduckgo.com/?q=!g+{{site.tag}})
<br>[![ip=:ip](https://img.shields.io/badge/IP-:ip-pink.svg?style=flat-square)](https://duckduckgo.com/?q=!g+:ip)
<br><span id=ipaddr><a href="//duckduckgo.com/?q=!g+:ip"><img src="https://img.shields.io/badge/IP-:ip-pink.svg?style=flat-square" alt=":ip" /></a></span>
<br>[![tics=:tics](https://img.shields.io/badge/tics-:tics-aquamarine.svg?style=flat-square)](https://duckduckgo.com/?q=!g+time::tics)




  

[vi]: {{DUCK}}=!g+favorite+editor+gvim
[md]: {{DUCK}}=!g+markdown

  
  
